t = gets.to_i

1.upto(t) do |i|
  given_string = gets
  last_word = ''

  given_string.each_char do |char|
    if last_word[0].nil?
      last_word << char
    elsif last_word[0] <= char
      last_word.prepend(char)
    else
      last_word << char
    end
  end

  puts "Case ##{i}: #{last_word}"
end
